<?php
/*
Template Name: Custom Sidebar Right
*/
?>			
<?php get_header() ?>
	<div class="wrapper-single-stripe">
		<div class="container-transparent">
			<?php the_title( '<h1 class="main-titles">', '</h1>' ); ?>
		</div>
	</div>
	<div class="wrapper-last">
		<div class="container1">
			<div class="two-thirds-left">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', 'page' ); ?>
				<?php endwhile; ?>	
			</div>
			<aside class="one-third-right">
				<?php get_sidebar('custom1'); ?>
			</aside>
			<?php get_footer(); ?>