<?php get_header() ?>
	<div class="wrapper-single-stripe">
		<div class="container-transparent">				
			<?php the_archive_title( '<h1 class="page-title">', '</h1>' ); the_archive_description( '<div class="taxonomy-description">', '</div>' ); ?>
		</div>
	</div>
	<div class="wrapper-last">
		<div class="container1">
			<div class="two-thirds-left">
				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part('content');?>
					<?php endwhile; ?>
					<div class="blog-index-post-nav-links">
						<div id="nav-older">
							<?php next_posts_link(__('&laquo; Older Posts' , 'clinic-stripes')); ?>
						</div>
						<div id="nav-newer">
							<?php previous_posts_link(__('Newer Posts &raquo;' , 'clinic-stripes')); ?>
						</div>
					</div>
				<?php else : ?>
					<?php get_template_part( 'content', 'none' ); ?>
				<?php endif; ?>
			</div>
			<div class="one-third-right">
				<?php get_sidebar(); ?>
			</div>
			<?php get_footer(); ?>