<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >
		<?php wp_head(); ?>
	</head>
	
	<body <?php body_class(); ?>>
	<?php do_action('clinic_stripes_after_body_tag'); ?>
	<div class="wrapper-head">	
		<div class="container-transparent">
			<header>				
				<div id="logo">
					<?php the_custom_logo();
						if (!has_custom_logo()) { ?>
								<div class="site-info-text">
									<div id="title-text">
										<a href="<?php echo esc_url(home_url('/')); ?>"><?php bloginfo( 'name'); ?></a>
									</div>
									<div id="description-text">
										<a href="<?php echo esc_url(home_url('/')); ?>"><?php bloginfo ( 'description'); ?></a>
									</div>
								</div> 
						<?php
					}?>
				</div>				
				<div class="info-box" id="address">
					<h4><?php _e('ADDRESS' , 'clinic-stripes'); ?></h4>
					<p><?php echo esc_html(get_theme_mod('street')); ?><br><?php echo esc_html(get_theme_mod('city_state_zip')); ?></p>
				</div>
				<div class="info-box" id="your-email">
					<h4><?php _e('EMAIL' , 'clinic-stripes'); ?></h4>
					<p><a href="mailto:<?php echo is_email(get_theme_mod('email')); ?>"><?php echo is_email(get_theme_mod('email')); ?></a></p>
				</div>
				<div class="info-box" id="phone">
					<h4><?php _e('PHONE' , 'clinic-stripes'); ?></h4>
					<p><a href="tel:<?php echo esc_attr(get_theme_mod('phone_link')); ?>"><?php echo esc_html(get_theme_mod('phone')); ?></a></p>
				</div>
			</header>
		</div>
	</div>
	<div class="wrapper-nav">
		<div class="container-nav">
			<?php // do_action('clinic_stripes_before_main_nav'); ?>
			<?php wp_nav_menu(array('theme_location' => 'mainnav', 'container_id' => 'main-navigation', 'fallback_cb' => 'clinic_stripes_default_menu')); ?>
			<?php // do_action('clinic_stripes_after_main_nav'); ?>
		</div>
	</div>