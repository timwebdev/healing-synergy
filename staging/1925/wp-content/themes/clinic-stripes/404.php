<?php get_header() ?>
	<div class="wrapper-last">
		<div class="container1">
			<article class="two-thirds-left">
				<div id="four-o-four-image">
					<?php if ( get_theme_mod('fourofour_image' , get_template_directory_uri() . '/images/whoops.png') ) : ?>
						<img src="<?php echo esc_url(get_theme_mod('fourofour_image' , get_template_directory_uri() . '/images/whoops.png')); ?>" alt="page-not-found">
					<?php endif; ?>
				</div>
				<?php _e('The page you are looking for is not available.' , 'clinic-stripes'); ?><br><br>
				<h2><?php _e('Please visit our ' , 'clinic-stripes'); ?><a href="<?php echo esc_url(home_url('/')); ?>"><?php _e('home page' , 'clinic-stripes'); ?></a></h2>
			</article>	
			<div class="one-third-right">
				<?php get_sidebar(); ?>
			</div>
<?php get_footer(); ?>