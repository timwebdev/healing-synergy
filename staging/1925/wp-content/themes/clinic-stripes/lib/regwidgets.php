<?php
function clinic_stripes_sidebars(){
	register_sidebar(array(
		'name' => __('Main Sidebar' , 'clinic-stripes'),
		'id' => 'sidebar1',
		'description' => __('This is the main sidebar' , 'clinic-stripes'),
		'before_widget' => '<aside class="widget %1$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h4>',
		'after_title' => '</h4>'
		)
	);	
	register_sidebar(array(
		'name' => __('Front Page Left Column' , 'clinic-stripes'),
		'id' => 'frontpageleftcolumn',
		'description' => __('This is the widget for the left column on the front page' , 'clinic-stripes'),
		'before_widget' => '<div class="widget %1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>'
		)
	);	
	register_sidebar(array(
		'name' => __('Front Page Middle Column' , 'clinic-stripes'),
		'id' => 'frontpagemiddlecolumn',
		'description' => __('This is the widget for the middle column on the front page' , 'clinic-stripes'),
		'before_widget' => '<div class="widget %1$s">',
		'after_widget' => '</div>',		
		'before_title' => '<h4>',
		'after_title' => '</h4>'
		)
	);
	register_sidebar(array(
		'name' => __('Front Page Right column' , 'clinic-stripes'),
		'id' => 'frontpagerightcolumn',
		'description' => __('This is the widget for the right column on the front page' , 'clinic-stripes'),
		'before_widget' => '<div class="widget %1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>'
		)
	);	
	register_sidebar(array(
		'name' => __('Footer Widget' , 'clinic-stripes'),
		'id' => 'footerwidget1',
		'description' => __('This is the footer widget. A good thing to put here would be your social media links. You can download the social media widget plugin from ChiroLoop\'s suggested plugins, and then drag it into this area.' , 'clinic-stripes'),
		'before_widget' => '<div class="widget %1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>'
		)
	);	
	register_sidebar(array(
		'name' => __('About Sidebar' , 'clinic-stripes'),
		'id' => 'aboutbar',
		'description' => __('This is the About sidebar. You need to first create a custom menu in the Menu area (Appearance --> Menus). After that, come back here and add a custom menu widget here, then assign the menu you just creaeted to that widget. Whatever pages you add to that custom menu will then show in your About side-bar.' , 'clinic-stripes'),
		'before_widget' => '<div class="widget %1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>'
		)
	);
	register_sidebar(array(
		'name' => __('Appointment Sidebar' , 'clinic-stripes'),
		'id' => 'appointmentbar',
		'description' => __('This is the Appointment sidebar. You need to use a plugin such as FS Contact Form (Fast Secure Contact Form). Add a text widget here, and then put the contact form shortcode in the text widget.' , 'clinic-stripes'),
		'before_widget' => '<div class="widget %1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>'
		)
	);	
	register_sidebar(array(
		'name' => __('New Patient Sidebar' , 'clinic-stripes'),
		'id' => 'newpatientbar',
		'description' => __('This is the New Patient sidebar. You need to first create a custom menu in the Menu area (Appearance --> Menus). After that, come back here and add a custom menu widget here, then assign the menu you just creaeted to that widget. Whatever pages you add to that custom menu will then show in your New Patient side-bar.' , 'clinic-stripes'),
		'before_widget' => '<div class="widget %1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>'
		)
	);
	register_sidebar(array(
		'name' => __('Custom Sidebar 1' , 'clinic-stripes'),
		'id' => 'custom1',
		'description' => __('Some people like to have more than one sidebar, so they can have different widgets on different pages of their sites. Whatever you place in this sidebar will show on pages assigned to the Custom Sidebar Template' , 'clinic-stripes'),
		'before_widget' => '<div class="widget %1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>'
		)
	);
}
add_action( 'widgets_init', 'clinic_stripes_sidebars' );