<?php

function clinic_stripes_scripts() {
	wp_enqueue_style( 'clinic-stripes-style', get_stylesheet_uri() );

	wp_enqueue_script( 'menu-dropdown', get_template_directory_uri() . '/js/menu-dropdown.js', array('jquery'), '20150422', true );	
	
	if ( is_singular() ) wp_enqueue_script( 'comment-reply' );
}
add_action( 'wp_enqueue_scripts', 'clinic_stripes_scripts' );

function clinic_stripes_enqueue_style($hook) {
 
	if( $hook != 'appearance_page_premium-upgrade') 
		return;
	wp_register_style( 'custom_admin_css', get_template_directory_uri() . '/upgrade/upgrade-page-style.css', false, '20151208' );
	wp_enqueue_style( 'custom_admin_css' );
}
add_action('admin_enqueue_scripts', 'clinic_stripes_enqueue_style');

//Display upgrade notice on customizer page
 
function clinic_stripes_upsell_notice() {

	// Enqueue the script
	wp_enqueue_script(
		'prefix-customizer-upsell',
		get_template_directory_uri() . '/js/upsell.js',
		array(), '1.0.0',
		true
	);

	// Localize the script
	wp_localize_script(
		'prefix-customizer-upsell',
		'prefixL10n',
		array(
			'prefixURL'	=> esc_url( '/wp-admin/themes.php?page=premium-upgrade.php' ),
			'prefixLabel'	=> __( 'New Pro Options', 'clinic-stripes' ),
		)
	);

}
add_action( 'customize_controls_enqueue_scripts', 'clinic_stripes_upsell_notice' );