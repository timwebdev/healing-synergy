<?php

function clinic_stripes_my_admin_menu() {
	add_theme_page( __('Upgrade to Clinic Stripes Premium' , 'clinic-stripes'),  __('New Pro Options' , 'clinic-stripes'), 'edit_theme_options', '/premium-upgrade.php', 'clinic_stripes_premium_options_page');
}
add_action( 'admin_menu', 'clinic_stripes_my_admin_menu' );

function clinic_stripes_premium_options_page(){
	?>
	<div class="upgrade-wrap">	
			
		<div class="clinic-stripes-option">
			<h1><?php _e('Want to know what you can do with the discounted upgrade to Pro?' , 'clinic-stripes'); ?></h1>
			<h2><?php _e('Just scroll down and check out all of pro options' , 'clinic-stripes'); ?></h2>
			<h1><?php _e('Easily Add Video to your Home Page Without Knowing Any Code' , 'clinic-stripes'); ?></h1>
			<div class="prem-screenshots">
				<img src="<?php echo esc_url(get_template_directory_uri() . '/upgrade/images/can-do-youtube.png'); ?>" />
			</div>
			<p><?php _e('Just drop an embed code directly into the box in the theme customizer.' , 'clinic-stripes'); ?><p>
		</div>
		<div class="clinic-stripes-option">
			<h1><?php _e('Integrates with Soliloquy Sliders Lite and Soliloquy Premium' , 'clinic-stripes'); ?></h1>
			<div class="prem-screenshots">
				<img src="<?php echo esc_url(get_template_directory_uri() . '/upgrade/images/soliloquy-sliders.png'); ?>" />
			</div>
			<p><?php _e('Soliloquy <a href="https://wordpress.org/plugins/soliloquy-lite/" target="blank">Lite is FREE</a>, Premium is $19 check out premium <a href="http://soliloquywp.com/" target="blank">here</a>.' , 'clinic-stripes'); ?><p>
		</div>
		<div class="clinic-stripes-option">
			<h1><?php _e('Make your slideshow, then just drop the ID number in the Customizer' , 'clinic-stripes'); ?></h1>
			<div class="prem-screenshots">
				<img src="<?php echo esc_url(get_template_directory_uri() . '/upgrade/images/slider-screen.png'); ?>" />
			</div>
			<p><?php _e('Soliloquy <a href="https://wordpress.org/plugins/soliloquy-lite/" target="blank">Lite is FREE</a>, Premium is $19 check out premium <a href="http://soliloquywp.com/" target="blank">here</a>.' , 'clinic-stripes'); ?><p>
		</div>
		<div class="clinic-stripes-option">
			<h1><?php _e('Retina Ready!' , 'clinic-stripes'); ?></h1>
			<div class="prem-screenshots">
				<img src="<?php echo esc_url(get_template_directory_uri() . '/upgrade/images/retina.png'); ?>" />
			</div>
			<p><?php _e("Don't want to look outdated on iPads, iMacs and MacBooks? The premium version includes the Retinal.js script which you can use to include your high resolution images." , 'clinic-stripes'); ?></p>
		</div>	
		<div class="clinic-stripes-option">
			<h1><?php _e('Extra Sidebars' , 'clinic-stripes'); ?></h1>
			<div class="prem-screenshots">
				<img src="<?php echo esc_url(get_template_directory_uri() . '/upgrade/images/custom-bars.jpg'); ?>" />
			</div>
			<p><?php _e("More flexibility with sidebar options allows for different sidbars on different pages." , 'clinic-stripes'); ?></p>
		</div>	
		<div class="clinic-stripes-option">
			<h1><?php _e('Office Hours Shortcode' , 'clinic-stripes'); ?></h1>
			<img src="<?php echo esc_url(get_template_directory_uri() . '/upgrade/images/hours.jpg'); ?>" />
			<p><?php _e('This allows you to just write "[hours]" on any page, post, or in a text widget, and the theme will create an office hours table like pictured here. Then you can edit the hours using the theme customizer. If you display your office hours on multiple posts, pages or widgets, changes in the customizer will be displayed all over your site.' , 'clinic-stripes'); ?></p>
		</div>		
		<div class="clinic-stripes-option">
			<h1><?php _e('Optional Footer Text' , 'clinic-stripes'); ?></h1>
			<p><?php _e('Adds a Footer Text section to the customizer so you can add an address, copyright, or even drop in your Schema Markup (for search engines) without having to edit the templates.' , 'clinic-stripes'); ?></p>
		</div>
		<div class="clinic-stripes-option">
			<h1><?php _e('Hide the Credit Link (White-Label it)' , 'clinic-stripes'); ?></h1>
			<img src="<?php echo esc_url(get_template_directory_uri() . '/upgrade/images/footer-link.png'); ?>" />
			<p><?php _e('Do you make sites for clients? Or do you just want to get rid of the "Created by Clinic Website Systems" link in the footer. Paid versions have the option to check a box to easily remove it.' , 'clinic-stripes'); ?></p>
		</div>		
		<div class="clinic-stripes-option">
			<h1><?php _e('Go Here for Special Pricing' , 'clinic-stripes'); ?></h1>
			<p><?php _e('We have special pricing for upgraders from the free theme. Just <a href="http://wpclinicthemes.com/special-offer-clinic-stripes-premium-r439dc69jr8dp6/" target="blank">click here</a> to find out how much.' , 'clinic-stripes'); ?></p>
		</div>
		
	</div>
	<?php
}