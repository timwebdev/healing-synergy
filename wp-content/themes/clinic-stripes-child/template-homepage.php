<?php
/*
Template Name: homepage-new
*/
?>
<?php get_header() ?>
<div id="home-slider">
<?php
echo do_shortcode('[smartslider3 slider=2]');
?>
</div>
	<div class="wrapper-single-stripe">
		<div class="container-transparent">
			<?php the_title( '<h1 class="main-titles">', '</h1>' ); ?>
		</div>
	</div>
	<div class="wrapper-last">
	
		<div class="container1">
			<div class="single-column">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', 'page' ); ?>
				<?php endwhile; ?>
			</div>
			<?php get_footer(); ?>
