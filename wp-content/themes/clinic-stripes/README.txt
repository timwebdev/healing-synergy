=== Clinic Stripes ===
Tags: black, blue, brown, gray, green, orange, pink, purple, red, silver, tan, white, yellow, dark, light, left-sidebar, right-sidebar, fluid-layout, responsive-layout, custom-colors, custom-menu, editor-style, featured-images, full-width-template, sticky-post, theme-options, threaded-comments, translation-ready
License: GNU General Public License v3
License URI: http://www.gnu.org/licenses/gpl.html

== Description ==
Physical Therapy theme with with many options for customization. Great for chiropractic, acupuncture, massage therapy, naturopathic or dental.

== Frequently Asked Questions ==

= How to I use this theme =

1. 	Upload the clinic-stripes.zip file from within the Themes area of your WordPress dashboard.

2. 	Select Clinic Stripes and click activate.

3.	Go to http://physicaltherapythemes.com and watch the videos related to your theme.

== Changelog ==

= 1.09 =
* Fixed the sidebar containers

= 1.08 =
* Updated the upgrad page with new options
* Updated the language file

= 1.07 =
* Updated upgrade page with new options.

= 1.06 =
* Edited upgrade page.

= 1.05 =
* Theme live on WordPress.org.

= 1.0 =
* Submitted for theme review.

== Upgrade Notice ==

= 1.0 =
* First edition

= 1.0.6 =
* Edited upgrade admin page.