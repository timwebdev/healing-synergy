<?php

if ( ! isset( $content_width ) ) {
	$content_width = 630; 
}

if ( ! function_exists( 'clinic_stripes_setup' ) ) :
function clinic_stripes_setup() {
	load_theme_textdomain( 'clinic-stripes', get_template_directory() . '/languages' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support('editor_style');
	add_theme_support('add_editor_style');
	add_theme_support( 'custom-logo' );
	
	// Register the Main Navigation Menu
	register_nav_menus( array(
		'mainnav' => __( 'Main Navigation', 'clinic-stripes' ),
	) );
	
	//Switch default core markup for search form, comment form, and comments to output valid HTML5.
	 add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );
}
endif; // clinic_stripes_setup
add_action( 'after_setup_theme', 'clinic_stripes_setup' );

// Fallback menu called when user forgets to assign a menu to Main Navigation in menu settings
function clinic_stripes_default_menu() {
    require (get_template_directory() . '/lib/default_menu.php' );
}

//Registers the sidebars and footers
require (get_template_directory() . '/lib/regwidgets.php');

//Load the customizer
require (get_template_directory() . '/lib/customizer.php');

//Suggest plugins for use by the theme
require (get_template_directory() . '/lib/suggested_plugins.php');

//Suggest Upgrade
require (get_template_directory() . '/upgrade/upgrade.php');

//Enque the stylesheets and scripts
require (get_template_directory() . '/lib/styles_and_scripts.php');

if ( ! function_exists( 'chiro_stripes_the_custom_logo' ) ) :
/**
 * Displays the optional custom logo.
 *
 * Does nothing if the custom logo is not available.
 *
 * @since Twenty Sixteen 1.2
 */
function chiro_stripes_the_custom_logo() {
	if ( function_exists( 'the_custom_logo' ) ) {
		the_custom_logo();
	}
}
endif;