<?php get_header() ?>
	<div class="wrapper-single-stripe">
		<div class="container-transparent">
			<?php the_title( '<h1 class="main-titles">', '</h1>' ); ?>
		</div>
	</div>
	<div class="wrapper-last">
		<div class="container1">
			<div class="two-thirds-left">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', 'page' ); ?>
					<?php
						// If comments are open or we have at least one comment, load up the comment template
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
					?>
				<?php endwhile; ?>
			</div>
			<div class="one-third-right">
				<?php get_sidebar(); ?>
			</div>
<?php get_footer(); ?>
