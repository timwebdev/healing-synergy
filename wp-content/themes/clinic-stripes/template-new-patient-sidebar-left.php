<?php
/*
Template Name: New Patient Sidebar Left
*/
?>
<?php get_header() ?>
	<div class="wrapper-single-stripe">
		<div class="container-transparent">
			<?php the_title( '<h1 class="main-titles">', '</h1>' ); ?>
		</div>
	</div>
	<div class="wrapper-last">
		<div class="container1">
			<div class="three-quarters-right">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', 'page' ); ?>
				<?php endwhile; ?>	
			</div>
			<aside class="one-quarter-left" id="gray-sidebars">
				<?php get_sidebar('newpatientbar'); ?>
			</aside>
			<?php get_footer(); ?>