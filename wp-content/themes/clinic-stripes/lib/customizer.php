<?php

// Navigation Menu Colors

add_action('customize_register', 'clinic_stripes_nav_colors');
function clinic_stripes_nav_colors($wp_customize){
	$wp_customize->add_section('nav_colors', array(
		'title' => __( 'Navigation Menu Colors' , 'clinic-stripes' ),
		'priority' => 145,
		'input_attrs' => array(
			'class' => 'whatever',
			'style' => 'color: #0a0',
		),
	)
	);
	
// Navigation background color

	$wp_customize->add_setting('navigation_background', array(
		'default' => '#393939',
		'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize, 'navigation_background', array(
		'label' => __('Background Color' , 'clinic-stripes'),
		'section' => 'nav_colors',
		'settings' => 'navigation_background',
		)
		)
	);
	
//  Navigation Text Color
	
	$wp_customize->add_setting('navigation_text_color', array(
		'default' => '#fff',
		'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize, 'navigation_text_color', array(
		'label' => __('Text Color' , 'clinic-stripes'),
		'section' => 'nav_colors',
		'settings' => 'navigation_text_color',
		)
		)
	);
	
//  Navigation Menu Item on Hover
	
	$wp_customize->add_setting('navigation_color_on_hover', array(
		'default' => '#565656',
		'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize, 'navigation_color_on_hover', array(
		'label' => __('Color on Mouse Hover' , 'clinic-stripes'),
		'section' => 'nav_colors',
		'settings' => 'navigation_color_on_hover',
		)
		)
	);
}

// Main Headline Control

add_action('customize_register', 'clinic_stripes_headline_control');
function clinic_stripes_headline_control($wp_customize){
	$wp_customize->add_section('headline_control', array(
		'title' => __('Main Headline Control' , 'clinic-stripes'),
		'priority' => 155,
		)
	);
	
// First half of headline text control

	$wp_customize->add_setting('headline_top_text', array(
		'default' => __('Insert your main tagline or headline' , 'clinic-stripes'),
		'sanitize_callback' => 'sanitize_text_field',
		
		)
	);
	$wp_customize->add_control('headline_top_text', array(
		'label' => __('Headline text first-half', 'clinic-stripes'),
		'section' => 'headline_control',
		'type' => 'text',
		'priority' => 1,
		)
	);
	
// First half of headline font size control

	$wp_customize->add_setting('headline_top_font_size', array(
		'default' => '50',
		'sanitize_callback' => 'sanitize_text_field',
		)
	);
	$wp_customize->add_control('headline_top_font_size', array(
		'label' => __('Headline font-size first-half *number only' , 'clinic-stripes'),
		'section' => 'headline_control',
		'type' => 'text',
		'priority' => 2,
		)
	);
	
// First half of main headline color control
	
	$wp_customize->add_setting('headline_top_color', array(
		'default' => '#3ba7bf',
		'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize, 'headline_top_color', array(
		'label' => __('Headline color first-half' , 'clinic-stripes'),
		'section' => 'headline_control',
		'settings' => 'headline_top_color',
		'priority' => 3,
		)
		)
	);
	
// Second half of headline text control

	$wp_customize->add_setting('headline_bottom_text', array(
		'default' => __('here easily' , 'clinic-stripes'),
		'sanitize_callback' => 'sanitize_text_field',		
		)
	);
	$wp_customize->add_control('headline_bottom_text', array(
		'label' => __('Headline text second-half' , 'clinic-stripes'),
		'section' => 'headline_control',
		'type' => 'text',
		'priority' => 4,
		)
	);
	
// Second half of headline font size control

	$wp_customize->add_setting('headline_bottom_font_size', array(
		'default' => '50',		
		'sanitize_callback' => 'sanitize_text_field',
		)
	);
	$wp_customize->add_control('headline_bottom_font_size', array(
		'label' => __('Headline font-size second-half *number only' , 'clinic-stripes'),
		'section' => 'headline_control',
		'type' => 'text',
		'priority' => 5,
		)
	);
	
// Second half of main headline color control
	
	$wp_customize->add_setting('headline_bottom_color', array(
		'default' => '#555',
		'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize, 'headline_bottom_color', array(
		'label' => __('Headline color second-half' , 'clinic-stripes'),
		'section' => 'headline_control',
		'settings' => 'headline_bottom_color',
		'priority' => 6,
		)
		)
	);
	
// Control of front headline button text

	$wp_customize->add_setting('headline_button_text', array(
		'default' => __('Request an Appointment' , 'clinic-stripes'),
		'sanitize_callback' => 'sanitize_text_field',
		)
	);
	$wp_customize->add_control('headline_button_text', array(
		'label' => __('Headline button text' , 'clinic-stripes'),
		'section' => 'headline_control',
		'type' => 'text',
		'priority' => 7,
		)
	);
	
// Front page headline button link

		$wp_customize->add_setting('headline_button_link', array(
		'default' => home_url('/contact'),
		'sanitize_callback' => 'esc_url_raw',
		)
	);
		$wp_customize->add_control('headline_button_link', array(
		'label' => __('Headline button link' , 'clinic-stripes'),
		'section' => 'headline_control',
		'type' => 'text',
		'priority' => 8,
		)
	);
}

// Background Colors

add_action('customize_register', 'clinic_stripes_global_changes');
function clinic_stripes_global_changes($wp_customize){
	$wp_customize->add_section('global_changes', array(
		'title' => __('Background Colors' , 'clinic-stripes'),
		'priority' => 135,
		)
	);
	
// Changes the header background color

	$wp_customize->add_setting('header_background', array(
		'default' => '#f2f2f2',
		'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize, 'header_background', array(
		'label' => __('Header Background Color' , 'clinic-stripes'),
		'section' => 'global_changes',
		'settings' => 'header_background',
		)
		)
	);
	
// Changes the website background color

	$wp_customize->add_setting('background_setting', array(
		'default' => '#ffffff',
		'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize, 'background_setting', array(
		'label' => __('Site Background Color' , 'clinic-stripes'),
		'section' => 'global_changes',
		'settings' => 'background_setting',
		)
		)
	);
	
// Changes the color of the containter

	$wp_customize->add_setting('box_setting', array(
		'default' => '#fff',
		'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize, 'box_setting' , array(
		'label' => __('Container Background Color' , 'clinic-stripes'),
		'section' => 'global_changes',
		'settings' => 'box_setting',
		'sanitize_callback' => 'sanitize_hex_color',
		)
		)
	);
	
// Changes the color of the sripes on all of the pages

	$wp_customize->add_setting('stripe_color', array(
		'default' => '#f2f2f2',
		'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize, 'stripe_color' , array(
		'label' => __('Color of stripes' , 'clinic-stripes'),
		'section' => 'global_changes',
		'settings' => 'stripe_color',
		)
		)
	);
	
// Changes the color of the sripe border on the front page

	$wp_customize->add_setting('stripe_border', array(
		'default' => '#3ba7bf',
		'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize, 'stripe_border' , array(
		'label' => __('Stripe border color (front page)' , 'clinic-stripes'),
		'section' => 'global_changes',
		'settings' => 'stripe_border',
		)
		)
	);
	
// Changes the color of the footer background

	$wp_customize->add_setting('foot_color', array(
		'default' => '#eeeeee',
		'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize, 'foot_color' , array(
		'label' => __('Footer background color' , 'clinic-stripes'),
		'section' => 'global_changes',
		'settings' => 'foot_color',
		)
		)
	);
}

// Site Colors

add_action('customize_register', 'clinic_stripes_site_colors');
function clinic_stripes_site_colors($wp_customize){
	$wp_customize->add_section('site_colors', array(
		'title' => __('Site Colors' , 'clinic-stripes'),
		'priority' => 140,
		)
	);
	
// phone, email, address color
	
	$wp_customize->add_setting('top_info_boxes', array(
		'default' => '#5a5af2',
		'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize, 'top_info_boxes', array(
		'label' => __('Phone, Email, Address text' , 'clinic-stripes'),
		'section' => 'site_colors',
		'settings' => 'top_info_boxes',
		)
		)
	);
	
// Button colors

	$wp_customize->add_setting('button_colors', array(
		'default' => '#3ba7bf',
		'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize, 'button_colors', array(
		'label' => __('Button Color' , 'clinic-stripes'),
		'section' => 'site_colors',
		'settings' => 'button_colors',
		)
		)
	);
	
// Button color on mouse hover

	$wp_customize->add_setting('button_colors_hover', array(
		'default' => '#49cce9',
		'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize, 'button_colors_hover', array(
		'label' => __('Button Color on Mouse Hover' , 'clinic-stripes'),
		'section' => 'site_colors',
		'settings' => 'button_colors_hover',
		)
		)
	);
	
// Text in the front page stripe

	$wp_customize->add_setting('stripe_text_color', array(
		'default' => '#505050',
		'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize, 'stripe_text_color', array(
		'label' => __('Text inside the stripe' , 'clinic-stripes'),
		'section' => 'site_colors',
		'settings' => 'stripe_text_color',
		)
		)
	);
	
// Footer navigation color

	$wp_customize->add_setting('foot_navigation_color', array(
		'default' => '#000',
		'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_control(new WP_Customize_Color_Control( $wp_customize, 'foot_navigation_color', array(
		'label' => __('Footer navigation color' , 'clinic-stripes'),
		'section' => 'site_colors',
		'settings' => 'foot_navigation_color',
		)
		)
	);
}

// Theme Variables

add_action('customize_register', 'clinic_stripes_theme_variables');
function clinic_stripes_theme_variables($wp_customize){
	$wp_customize->add_section('theme_variables', array(
		'title' => __('Theme Variables' , 'clinic-stripes'),
		'priority' => 150,
		)
	);
	
// change the phone number

	$wp_customize->add_setting('phone', array(
		'default' => '555-555-5555',
		'sanitize_callback' => 'sanitize_text_field',
		)
	);
	$wp_customize->add_control('phone', array(
		'label' => __('Phone for display' , 'clinic-stripes'),
		'section' => 'theme_variables',
		'type' => 'text',
		'priority' => 1,
		)
	);
	
// change the phone number link

	$wp_customize->add_setting('phone_link', array(
		'default' => '5555555555',
		'sanitize_callback' => 'sanitize_text_field',
		)
	);
	$wp_customize->add_control('phone_link', array(
		'label' => __('Phone link (NO dashes or parentheses)' , 'clinic-stripes'),
		'section' => 'theme_variables',
		'type' => 'text',
		'priority' => 2,
		)
	);
	
// email address

	$wp_customize->add_setting('email', array(
		'default' => 'user@domain.com',
		'sanitize_callback' => 'sanitize_email',
		)
	);
	$wp_customize->add_control('email', array(
		'label' => __('Email address' , 'clinic-stripes'),
		'section' => 'theme_variables',
		'type' => 'text',
		'priority' => 3,
		)
	);
	
// street

	$wp_customize->add_setting('street', array(
		'default' => '500 Cherry St.',
		'sanitize_callback' => 'sanitize_text_field',
		)
	);
	$wp_customize->add_control('street', array(
		'label' => __('Street' , 'clinic-stripes'),
		'section' => 'theme_variables',
		'type' => 'text',
		'priority' => 4,
		)
	);
	
// City, state, zip

	$wp_customize->add_setting('city_state_zip', array(
		'default' => 'Sacramento, CA 95661',
		'sanitize_callback' => 'sanitize_text_field',
		)
	);
	$wp_customize->add_control('city_state_zip', array(
		'label' => __('City, State, Zip' , 'clinic-stripes'),
		'section' => 'theme_variables',
		'type' => 'text',
		'priority' => 5,
		)
	);

}

// Images

add_action('customize_register', 'clinic_stripes_images');
function clinic_stripes_images($wp_customize){
	$wp_customize->add_section('images', array(
		'title' => __('Images' , 'clinic-stripes'),
		'priority' => 160,
		)
	);
	
// The Main Front Page Image

	$wp_customize->add_setting('main_front_image', array(
		'default' => get_template_directory_uri() . '/images/main-image.png',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control(new WP_Customize_Image_Control( $wp_customize, 'main_front_image', array(
		'label' => __('Main Front Page Image' , 'clinic-stripes'),
		'section' => 'images',
		'settings' => 'main_front_image',
		)
		)
	);
	
// Text in the left image on the front page columns

	$wp_customize->add_setting('left_column_image_text', array(
		'default' => __('Latest News' , 'clinic-stripes'),
		'sanitize_callback' => 'sanitize_text_field',
		)
	);
	$wp_customize->add_control('left_column_image_text', array(
		'label' => __('The Text in the image below' , 'clinic-stripes'),
		'section' => 'images',
		'type' => 'text',
		)
	);
	
// Left image on front page columns

	$wp_customize->add_setting('left_column_image', array(
		'default' => get_template_directory_uri() . '/images/doc-with-spine.jpg',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control(new WP_Customize_Image_Control( $wp_customize, 'left_column_image', array(
		'label' => __('Left column image (312x150)' , 'clinic-stripes'),
		'section' => 'images',
		'settings' => 'left_column_image',
		)
		)
	);
	
// Text in the middle image on the front page columns

	$wp_customize->add_setting('middle_column_image_text', array(
		'default' => __('Hours of Operation' , 'clinic-stripes'),
		'sanitize_callback' => 'sanitize_text_field',
		)
	);
	$wp_customize->add_control('middle_column_image_text', array(
		'label' => __('The Text in image below' , 'clinic-stripes'),
		'section' => 'images',
		'type' => 'text',
		)
	);
	
// Middle image on front page columns

	$wp_customize->add_setting('middle_column_image', array(
		'default' => get_template_directory_uri() . '/images/hours.jpg',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control(new WP_Customize_Image_Control( $wp_customize, 'middle_column_image', array(
		'label' => __('Middle column image (312x150)' , 'clinic-stripes'),
		'section' => 'images',
		'settings' => 'middle_column_image',
		)
		)
	);
	
// Text in the right image on the front page columns

	$wp_customize->add_setting('right_column_image_text', array(
		'default' => __('Interactive Map' , 'clinic-stripes'),
		'sanitize_callback' => 'sanitize_text_field',
		)
	);
	$wp_customize->add_control('right_column_image_text', array(
		'label' => __('The Text in image below' , 'clinic-stripes'),
		'section' => 'images',
		'type' => 'text',
		)
	);
	
// Right image on front page columns

	$wp_customize->add_setting('right_column_image', array(
		'default' => get_template_directory_uri() . '/images/pin-map.jpg',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control(new WP_Customize_Image_Control( $wp_customize, 'right_column_image', array(
		'label' => __('Right column image (312x150)' , 'clinic-stripes'),
		'section' => 'images',
		'settings' => 'right_column_image',
		)
		)
	);
	
// 404 image
	
	$wp_customize->add_setting('fourofour_image', array(
		'default' => get_template_directory_uri() . '/images/whoops.png',
		'sanitize_callback' => 'esc_url_raw',
		)
	);
	$wp_customize->add_control(new WP_Customize_Image_Control( $wp_customize, 'fourofour_image', array(
		'label' => __('Page not found image' , 'clinic-stripes'),
		'section' => 'images',
		'settings' => 'fourofour_image',
		)
		)
	);
	

}

// Columns in Stripe

add_action('customize_register', 'clinic_stripes_front_page_stripe_text_left');
function clinic_stripes_front_page_stripe_text_left($wp_customize){

	$wp_customize->add_panel( 'column_panel', array(
		'title' => __('Columns in the Stripe' , 'clinic-stripes'),
		'priority' => 165,
		)
	);

	$wp_customize->add_section('front_page_stripe_text_left', array(
		'title' => __('Left Column' , 'clinic-stripes'),
		'priority' => 1,
		'panel' => 'column_panel',
		)
	);
	
// Left column heading

	$wp_customize->add_setting('stripe_left_column_heading', array(
		'default' => __('Our Team' , 'clinic-stripes'),
		'sanitize_callback' => 'sanitize_text_field',
		)
	);
	$wp_customize->add_control('stripe_left_column_heading', array(
		'label' => __('Left column heading' , 'clinic-stripes'),
		'section' => 'front_page_stripe_text_left',
		'type' => 'text',
		'priority' => 1,
		)
	);
	
// Left column sub heading

		$wp_customize->add_setting('stripe_left_column_sub_heading', array(
		'default' => __('Learn about the doctor and staff' , 'clinic-stripes'),
		'sanitize_callback' => 'sanitize_text_field',
		)
	);
		$wp_customize->add_control('stripe_left_column_sub_heading', array(
		'label' => __('Left column sub-heading' , 'clinic-stripes'),
		'section' => 'front_page_stripe_text_left',
		'type' => 'text',
		'priority' => 2,
		)
	);
	
// Left column body text

		$wp_customize->add_setting('stripe_left_column_body', array(
		'default' => __('click here for information regarding the team that is ready to serve you' , 'clinic-stripes'),
		'sanitize_callback' => 'sanitize_text_field',
		)
	);
		$wp_customize->add_control('stripe_left_column_body', array(
		'label' => __('Left column body text' , 'clinic-stripes'),
		'section' => 'front_page_stripe_text_left',
		'type' => 'text',
		'priority' => 3,
		)
	);
	
// Left column link

		$wp_customize->add_setting('stripe_left_column_link', array(
		'default' => home_url('/about'),
		'sanitize_callback' => 'esc_url_raw',
		)
	);
		$wp_customize->add_control('stripe_left_column_link', array(
		'label' => __('Left column link' , 'clinic-stripes'),
		'section' => 'front_page_stripe_text_left',
		'type' => 'text',
		'priority' => 4,
		)
	);	
		
// Middle column heading

	$wp_customize->add_section('front_page_stripe_text_middle', array(
		'title' => __('Middle Column' , 'clinic-stripes'),
		'priority' => 2,
		'panel' => 'column_panel',
		)
	);

	$wp_customize->add_setting('stripe_middle_column_heading', array(
		'default' => __('New Patient Forms' , 'clinic-stripes'),
		'sanitize_callback' => 'sanitize_text_field',
		)
	);
	$wp_customize->add_control('stripe_middle_column_heading', array(
		'label' => __('Middle column heading' , 'clinic-stripes'),
		'section' => 'front_page_stripe_text_middle',
		'type' => 'text',
		'priority' => 1,
		)
	);
	
// Middle column sub heading

		$wp_customize->add_setting('stripe_middle_column_sub_heading', array(
		'default' => __('Print forms ahead of time' , 'clinic-stripes'),
		'sanitize_callback' => 'sanitize_text_field',
		)
	);
		$wp_customize->add_control('stripe_middle_column_sub_heading', array(
		'label' => __('Middle column sub-heading' , 'clinic-stripes'),
		'section' => 'front_page_stripe_text_middle',
		'type' => 'text',
		'priority' => 2,
		)
	);
	
// Middle column body text

		$wp_customize->add_setting('stripe_middle_column_body', array(
		'default' => __('Print forms ahead of time, and complete them at your own convenience.' , 'clinic-stripes'),
		'sanitize_callback' => 'sanitize_text_field',
		)
	);
		$wp_customize->add_control('stripe_middle_column_body', array(
		'label' => __('Middle column body text' , 'clinic-stripes'),
		'section' => 'front_page_stripe_text_middle',
		'type' => 'text',
		'priority' => 3,
		)
	);
	
// Middle column link

		$wp_customize->add_setting('stripe_middle_column_link', array(
		'default' => home_url('/forms'),
		'sanitize_callback' => 'esc_url_raw',
		)
	);
		$wp_customize->add_control('stripe_middle_column_link', array(
		'label' => __('Middle column link' , 'clinic-stripes'),
		'section' => 'front_page_stripe_text_middle',
		'type' => 'text',
		'priority' => 4,
		)
	);
	
// Right column heading

	$wp_customize->add_section('front_page_stripe_text_right', array(
		'title' => __('Right Column' , 'clinic-stripes'),
		'priority' => 3,
		'panel' => 'column_panel',
		)
	);

	$wp_customize->add_setting('stripe_right_column_heading', array(
		'default' => __('Book it Online' , 'clinic-stripes'),
		'sanitize_callback' => 'sanitize_text_field',
		)
	);
	$wp_customize->add_control('stripe_right_column_heading', array(
		'label' => __('Right column heading' , 'clinic-stripes'),
		'section' => 'front_page_stripe_text_right',
		'type' => 'text',
		'priority' => 1,
		)
	);
	
// Right column sub heading

		$wp_customize->add_setting('stripe_right_column_sub_heading', array(
		'default' => __('Make a request here' , 'clinic-stripes'),
		'sanitize_callback' => 'sanitize_text_field',
		)
	);
		$wp_customize->add_control('stripe_right_column_sub_heading', array(
		'label' => __('Right column sub-heading' , 'clinic-stripes'),
		'section' => 'front_page_stripe_text_right',
		'type' => 'text',
		'priority' => 2,
		)
	);
	
// Right column body text

		$wp_customize->add_setting('stripe_right_column_body', array(
		'default' => __('You can make a request from your PC, tablet or smart phone.' , 'clinic-stripes'),
		'sanitize_callback' => 'sanitize_text_field',
		)
	);
		$wp_customize->add_control('stripe_right_column_body', array(
		'label' => __('Right column body text' , 'clinic-stripes'),
		'section' => 'front_page_stripe_text_right',
		'type' => 'text',
		'priority' => 3,
		)
	);
	
// Right column link

		$wp_customize->add_setting('stripe_right_column_link', array(
		'default' => home_url('/forms'),
		'sanitize_callback' => 'esc_url_raw',
		)
	);
		$wp_customize->add_control('stripe_right_column_link', array(
		'label' => __('Right column link' , 'clinic-stripes'),
		'section' => 'front_page_stripe_text_right',
		'type' => 'text',
		'priority' => 4,
		)
	);
}

// Side-bar button on templates

add_action('customize_register', 'cpro_sidebar_button');
function cpro_sidebar_button($wp_customize){
	
	$wp_customize->add_section('sidebar_button', array(
		'title' => __('Side-bar button on templates' , 'clinic-stripes'),
		'priority' => 170,
		)
	);
	
	// This controls the button text
	
	$wp_customize->add_setting('button_text', array(
		'default' => __('Request Appointment' , 'clinic-stripes'),
		'sanitize_callback' => 'sanitize_text_field',
		)
	);
	$wp_customize->add_control('button_text', array(
		'label' => __('Button Text' , 'clinic-stripes'),
		'section' => 'sidebar_button',
		'type' => 'text',
		'priority' => 1,
		)
	);
	
	// This controls the button link
	
	$wp_customize->add_setting('appointment_button_link', array(
		'default' => home_url('/contact'),
		'sanitize_callback' => 'esc_url_raw',
		)
	);
		$wp_customize->add_control('appointment_button_link', array(
		'label' => __('Appointment button link' , 'clinic-stripes'),
		'section' => 'sidebar_button',
		'type' => 'text',
		'priority' => 2,
		)
	);
}

add_action( 'customize_controls_print_styles', 'clinic_stripes_customizer_styles', 999 );

add_action('wp_footer', 'clinic_stripes_credit');
function clinic_stripes_credit() {
  $content = '<div class="footer-wrapper" id="clinicwebsitesolutions-credit" style="font-size:12px; padding:5px 10px; font-family:open_sansregular; position:relative; z-index:9" >Created by <a href="http://wpclinicthemes.com" rel="nofollow">Clinic Website Systems</div>';
  echo $content;  
}

// This uses the header hook to instert custom css
function clinic_stripes_customize_css()
{
    ?>
			
		<style type="text/css">
		
			.wrapper1, .wrapper-last, .wrapper-front-widgets{
				background-color:<?php echo esc_html(get_theme_mod('background_setting' , '#ffffff')); ?>;
			}
			.container1, .container-top-border, .container-front-widgets{
				background-color:<?php echo esc_html(get_theme_mod('box_setting' , '#fff')); ?>;
			}
			.wrapper-head{
				background-color:<?php echo esc_html(get_theme_mod('header_background' , '#f2f2f2')); ?>;
			}
			.wrapper-front-stripe, .wrapper-single-stripe{
				background-color:<?php echo esc_html(get_theme_mod('stripe_color' , '#f2f2f2')); ?>;
				border-bottom:5px solid <?php echo esc_html(get_theme_mod('stripe_border' , '#3ba7bf')); ?>;
			}
			.container-top-border{
				border-top:1px solid <?php echo esc_html(get_theme_mod('stripe_border' , '#3ba7bf')); ?>;
			}
			.footer-wrapper, body{
				background-color:<?php echo esc_html(get_theme_mod('foot_color' , '#eee')); ?>;
			}
			.wrapper-nav, #main-navigation a {
				background-color: <?php echo esc_html(get_theme_mod('navigation_background' , '#393939')); ?>;
			}
			#main-navigation li a {
				color: <?php echo esc_html(get_theme_mod('navigation_text_color' , '#ffffff')); ?>;
				background-color: <?php echo esc_html(get_theme_mod('navigation_background' , '#393939')); ?>;
			}
			#main-navigation li a:hover{
				background-color: <?php echo esc_html(get_theme_mod('navigation_color_on_hover' , '#565656')); ?>;
			}
			#foot-navigation li a, #chiroloop-credit, #chiroloop-credit a{
				color:<?php echo esc_html(get_theme_mod( 'foot_navigation_color' , '#000' )); ?>;
			}
			.info-box p, .info-box a{
				color:<?php echo esc_html(get_theme_mod('top_info_boxes' , '#5a5af2')); ?>;
			}
			#tagline-top{
				color:<?php echo esc_html(get_theme_mod('headline_text_top' , '#3ba7bf')); ?>;
			}
			#tagline-bottom{
				color:<?php echo esc_html(get_theme_mod('headline_text_bottom' , '#555')); ?>;
			}
			.site-buttons a, .front-headline-button a, #fscf_submit1, input.site-buttons{
				background-color:<?php echo esc_html(get_theme_mod('button_colors' , '#3ba7bf')); ?>;
			}
			.site-buttons a:hover, .front-headline-button a:hover, #fscf_submit1:hover, input.site-buttons:hover{
				background-color:<?php echo esc_html(get_theme_mod('button_colors_hover' , '#49cce9')); ?>;
			}
			#left.front-image-container{
				background-image: url('<?php echo esc_url_raw(get_theme_mod('left_column_image' , get_template_directory_uri() . '/images/doc-with-spine.jpg')); ?>');
			}
			#middle.front-image-container{
				background-image: url('<?php echo esc_url_raw(get_theme_mod('middle_column_image' , get_template_directory_uri() . '/images/hours.jpg')); ?>');
			}
			#right.front-image-container{
				background-image: url('<?php echo esc_url_raw(get_theme_mod('right_column_image' , get_template_directory_uri() . '/images/pin-map.jpg')); ?>');
			}
			.front-column-head, .front-column-sub-head, .front-column-body, h1.main-titles{
				color:<?php echo esc_html(get_theme_mod('stripe_text_color' , '#505050')); ?>;
			}	
			#tagline-top{
				color:<?php echo esc_html(get_theme_mod( 'headline_top_color' , '#3ba7bf' )); ?>;
				font-size:<?php echo esc_html(get_theme_mod( 'headline_top_font_size' , '50' )); ?>px;
			}
			#tagline-bottom{
				color:<?php echo esc_html(get_theme_mod( 'headline_bottom_color' , '#555' )); ?>;
				font-size:<?php echo esc_html(get_theme_mod( 'headline_bottom_font_size' , '50' )); ?>px;
			}		 
		</style>
    <?php
}
add_action( 'wp_head', 'clinic_stripes_customize_css');