<?php do_action('sidebar_begin'); ?>
<?php if ( ! dynamic_sidebar('appointmentbar')) : ?>
	<h2><?php _e('You need to add a text widget in the Appointment Sidebar widget area, then copy and paste your shortode from the FS Contact Form plugin.' , 'clinic-stripes'); ?></h2>
<?php endif; ?>
<?php do_action('sidebar_end'); ?>