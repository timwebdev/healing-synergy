		</div> <!-- End of the container div -->
	</div> <!-- End of wrapper div -->
	<div class="footer-wrapper">
		<footer>
			<div class="footer-widget-1">
				<?php if ( ! dynamic_sidebar('footerwidget1')) : ?>
				<h2><?php _e('Footer Widget' , 'clinic-stripes'); ?></h2>
				<?php endif; ?>
			</div>
			<div class="foot-container-nav">
				<?php wp_nav_menu(array('theme_location' => 'mainnav', 'container_id' => 'foot-navigation')); ?>
			</div>
			<?php do_action('below_footer_widgets'); ?>
		</footer>
	</div>
	<?php wp_footer(); ?>
</body>
</html>
