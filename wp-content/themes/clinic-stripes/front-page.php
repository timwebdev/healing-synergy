<?php if ( 'posts' == get_option( 'show_on_front' ) ) {
	include( get_home_template() );
	} else { ?>
		<?php get_header() ?>
			<div class="wrapper1">
				<div class="container1">
					<div id="front-page-headline">
						<span id="tagline-top"><?php echo esc_html(get_theme_mod('headline_top_text' , __('Insert your main tagline or headline' , 'clinic-stripes'))); ?></span>
						<span id="tagline-bottom"><?php echo esc_html(get_theme_mod( 'headline_bottom_text' , __('here easily' , 'clinic-stripes'))); ?></span>
						<div class="front-headline-button">
							<a href="<?php echo esc_url(get_theme_mod( 'headline_button_link' , esc_url(home_url('/contact'))) ); ?>"><?php echo esc_html(get_theme_mod('headline_button_text' , __('Request an appointment' , 'clinic-stripes'))); ?></a>
						</div>
					</div>						
					<div class="media-wrapper">
						<?php if ( get_theme_mod('main_front_image' , get_template_directory_uri() .'/images/main-image.png') ) : ?>
							<img src="<?php echo get_theme_mod('main_front_image' , get_template_directory_uri() .'/images/main-image.png'); ?>" />
						<?php endif; ?>
					</div>					
				</div>
			</div>
			<div class="wrapper-front-stripe">
				<div class="container-transparent">
					<div class="front-stripe-columns" id="front-column-left">
						<a href="<?php echo esc_url(get_theme_mod('stripe_left_column_link')); ?>">
							<div class="front-column-head"><?php echo esc_html(get_theme_mod('stripe_left_column_heading')); ?></div>
							<div class="front-column-sub-head"><?php echo esc_html(get_theme_mod('stripe_left_column_sub_heading')); ?></div>
							<div class="front-column-body"><?php echo esc_html(get_theme_mod('stripe_left_column_body')); ?></div>	
						</a>
					</div>
					<div class="front-stripe-columns" id="front-column-middle">
						<a href="<?php echo esc_url(get_theme_mod('stripe_middle_column_link')); ?>">
							<div class="front-column-head"><?php echo esc_html(get_theme_mod('stripe_middle_column_heading')); ?></div>
							<div class="front-column-sub-head"><?php echo esc_html(get_theme_mod('stripe_middle_column_sub_heading')); ?></div>
							<div class="front-column-body"><?php echo esc_html(get_theme_mod('stripe_middle_column_body')); ?></div>
						</a>
					</div>
					<div class="front-stripe-columns" id="front-column-right">
						<a href="<?php echo esc_url(get_theme_mod('stripe_right_column_link')); ?>">
							<div class="front-column-head"><?php echo esc_html(get_theme_mod('stripe_right_column_heading')); ?></div>
							<div class="front-column-sub-head"><?php echo esc_html(get_theme_mod('stripe_right_column_sub_heading')); ?></div>
							<div class="front-column-body"><?php echo esc_html(get_theme_mod('stripe_right_column_body')); ?></div>
						</a>
					</div>			
				</div>
			</div>
			<div class="wrapper-front-widgets">
				<div class="container-front-widgets">		
					<div class="front-page-column-widget-containers" id="front-container-left">
						<div class="front-image-container" id="left">
							<div class="image-holder">
								&nbsp;
							</div>
							<div class="image-text">
								<?php echo esc_html(get_theme_mod('left_column_image_text' , __('Latest News' , 'clinic-stripes'))); ?>
							</div>
						</div>
						<div class="front-column-widget">
							<?php if ( ! dynamic_sidebar('frontpageleftcolumn')) : ?>
								<h2><?php echo _e('Left Column Widget' , 'clinic-stripes'); ?></h2>
							<?php endif; ?>
						</div>
					</div>			
					<div class="front-page-column-widget-containers" id="front-container-middle">
						<div class="front-image-container" id="middle">
							<div class="image-holder">
								&nbsp;
							</div>
							<div class="image-text">
								<?php echo esc_html(get_theme_mod('middle_column_image_text' , __('Hours of Operation' , 'clinic-stripes'))); ?>
							</div>
						</div>
						<div class="front-column-widget">
							<?php if ( ! dynamic_sidebar('frontpagemiddlecolumn')) : ?>
								<h2><?php echo _e('Middle column widget' , 'clinic-stripes'); ?></h2>
							<?php endif; ?>
						</div>
					</div>			
					<div class="front-page-column-widget-containers" id="front-container-right">
						<div class="front-image-container" id="right">
							<div class="image-holder">
								&nbsp;
							</div>
							<div class="image-text">
								<?php echo esc_html(get_theme_mod('right_column_image_text' , __('Interactive Map' , 'clinic-stripes'))); ?>
							</div>
						</div>
						<div class="front-column-widget">
							<?php if ( ! dynamic_sidebar('frontpagerightcolumn')) : ?>
								<h2><?php echo _e('Right column widget' , 'clinic-stripes'); ?></h2>
							<?php endif; ?>
						</div>
					</div>		
				</div>		
			</div>	
			<div class="wrapper-last">
				<div class="container-top-border">
					<div class="one-column">
						<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'content', 'page' ); ?>
						<?php endwhile; ?>	
					</div>
					<?php get_footer(); ?>
	<?php
}; ?>